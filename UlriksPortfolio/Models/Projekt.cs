﻿using System.Data.Entity;

namespace UlriksPortfolio.Models
{
    public class Projekt
    {
        public int ID { get; set; }
        public string Titel { get; set; }
        public string Beskrivning { get; set; }
    }

    public class ProjektDBContext : DbContext
    {
        public DbSet<Projekt> Projekts { get; set; }
    }
}