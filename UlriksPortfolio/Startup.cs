﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UlriksPortfolio.Startup))]
namespace UlriksPortfolio
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
